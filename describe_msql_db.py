from collections import OrderedDict
from mysql import connector

conn = connector.connect(user=MSQL_USER,password=MSQL_PASSWORD,host='localhost',database='database_name')
cur = conn.cursor()
sql = "SHOW TABLES IN ccsoccer_copy"

cur.execute(sql)

tables = []
i = 0

for table in cur:
	tables.insert(i,table[0])
	i+=1

table_columns = OrderedDict()

for table in tables:
	sql = "DESCRIBE {}".format(table)
	cur.execute(sql)

	for column in cur:
		table_columns[column[0]] = column[1:]

table_info = {}
table_info[tables[0]] = table_columns

cur.close()
conn.close()
