from pathlib import Path

'''
Get list of directories under the parent directory you want to search.
'''
dirs = sorted(Path.cwd().glob('path/to/Parent/**/'))

# Map files to their parent directory.
sub_folders_dict = {}

# Use to store list of file names of parent directory.
temp_names = []

'''
Navigate through the file system under the parent above and gather the list of files under each directory.
'''
for item in dirs:
	if item.is_dir() and 'possible_exclusions' not in item.name:
		for path in sorted(Path.cwd().glob('path/to/Parent/{}/*.*'.format(item.name))):
			temp_names.append(path)

		sub_folders_dict[item.name] = temp_names
		temp_names = []

'''
Once we have all the files we want we can add requirements to rename all the files we want.
In my case, there were multiple directories full of files with hyphens in the filename (I
did not name them that way). xP
'''
for keys,values in sub_folders_dict.items():
	for item in values:
		if "-" in item.name:
			item.replace(item.parents[0].joinpath(item.name.replace("-","_")))
